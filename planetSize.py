#based on https://blog.michael.franzl.name/2016/11/30/venus-apparent-diameter-plot-2017-2020/

import datetime as dt
import ephem
import math
from math import radians as rad, degrees as deg
import matplotlib.pyplot as plt

venus = ephem.Venus()
jupiter = ephem.Jupiter()
saturn = ephem.Saturn()
mars = ephem.Mars()

venus_diameter_meters = 2 * 6051800
jupiter_diameter_meters = 142984000
saturn_diameter_meters = 2*58232000
mars_diameter_meters = 6792400
au_meters = 149597870700

t = dt.datetime.strptime("20220101", "%Y%m%d")

diff = dt.timedelta(days = 1)

distances_venus = []
distances_jupiter = []
distances_saturn = []
distances_mars = []
times = []

for i in range(0, int(16 * 365)):
    venus.compute(t)
    jupiter.compute(t)
    saturn.compute(t)
    mars.compute(t)
    
    dist_venus_meters = venus.earth_distance * au_meters
    dist_jupiter_meters = jupiter.earth_distance * au_meters
    dist_saturn_meters = saturn.earth_distance * au_meters
    dist_mars_meters = mars.earth_distance * au_meters
    
    angular_diameter_arcminutes_venus = 60 * deg(2 * math.asin(venus_diameter_meters / (2 * dist_venus_meters)))
    angular_diameter_arcminutes_jupiter = 60 * deg(2 * math.asin(jupiter_diameter_meters / (2 * dist_jupiter_meters)))
    angular_diameter_arcminutes_saturn = 60 * deg(2 * math.asin(saturn_diameter_meters / (2 * dist_saturn_meters)))
    angular_diameter_arcminutes_mars = 60 * deg(2 * math.asin(mars_diameter_meters / (2 * dist_mars_meters)))
    
    distances_venus.append(angular_diameter_arcminutes_venus)
    distances_jupiter.append(angular_diameter_arcminutes_jupiter)
    distances_saturn.append(angular_diameter_arcminutes_saturn)
    distances_mars.append(angular_diameter_arcminutes_mars)
    times.append(t)
    t += diff

plt.title("Apparent size")
plt.plot(times, distances_venus, label = "Venus")
plt.plot(times, distances_jupiter, label = "Jupiter")
plt.plot(times, distances_saturn, label = "Saturn")
plt.plot(times, distances_mars, label = "Mars")
plt.axvline(x=dt.datetime.today(), ls = "dashed", color = "grey")
plt.grid(axis = "x")
plt.xlabel("Date")
plt.ylabel("Diameter (arcmin)")
plt.legend()
plt.show()
